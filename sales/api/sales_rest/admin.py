from django.contrib import admin

from .models import Salesperson, Customer, Sale, AutomobileVO

# Register your models here.

@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    model = Salesperson
    list_display = (
        "first_name",
        "last_name",
        "employee_id",
    )

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    model = Customer
    list_display = (
        "first_name",
        "last_name",
        "address",
        "phone_number",
    )

@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    model = AutomobileVO
    list_display = (
        "import_href",
        "color",
        "year",
        "vin",
        "sold",
    )

@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    model = Sale
    list_display = (
        "automobile",
        "salesperson",
        "customer",
        "price",
    )
