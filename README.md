# CarCar

Team:

* Person 1 - Clement Lee - SALES
* Person 2 - Gary Burns - SERVICE

## How to Run this App
    Upon making sure docker and docker-compose are both installed and present, run -
    `docker volume create project-beta-pgdata`
    This creates the volume for docker to use to hold our image.
    To create the image -
    `docker-compose build`
    Finally, to start the containers from the image -
    `docker-compose up`

    Now,to access the React frontend to manipulate our data, open a browser and navigate to -
    `http://localhost:3000/`

## Design
    CarCar is a project that interfaces with multiple microservices to enable the administration of a car dealership.
    The microservices utilized are:

    SERVICE
     -Appointments, Technicians
     -Automobile Value Object from Inventory via http://inventory-api:8000/automobile

    SALES
     -Customer, Sales, Salesperson
     -Automobile Value Object from Inventory via http://inventory-api:8000/automobile

    INVENTORY
     -Manufacturer, VehicleModel, Automobile

    It uses a React Frontend to display all data.




    Diagram:
    ![Img](https://i.imgur.com/gnnIkEw.png)

## Service microservice/API

    Appointment model - Holds date_time, reason, status, VIN, customer, and technician.
    Technician model - Holds first_name, last_name, employee_id
    AutomobileVO - generates VO based on Automobile model in inventory, holds VIN and sold boolean

## Sales microservice/API

    Customer model - Holds first_name, last_name, address, and phone_number.
    Sale model - Holds automobile foreign key, salesperson foreign key, customer foreign key and price.
    AutomobileVO - generates VO based on Automobile model in inventory, holds VIN and sold boolean

## Requests Chart

____________________________________________________________________________________________________
| Action                             | Method | URL                                                |
|------------------------------------|--------|----------------------------------------------------|
| List manufacturers                 | GET    | http://localhost:8100/api/manufacturers/           |
| Create a manufacturer              | POST   | http://localhost:8100/api/manufacturers/           |
| Get a specific manufacturer        | GET    | http://localhost:8100/api/manufacturers/:id/       |
| Update a specific manufacturer     | PUT    | http://localhost:8100/api/manufacturers/:id/       |
| Delete a specific manufacturer     | DELETE | http://localhost:8100/api/manufacturers/:id/       |
| List vehicle models                | GET    | http://localhost:8100/api/models/                  |
| Create a vehicle model             | POST   | http://localhost:8100/api/models/                  |
| Get a specific vehicle model       | GET    | http://localhost:8100/api/models/:id/              |
| Update a specific vehicle model    | PUT    | http://localhost:8100/api/models/:id/              |
| Delete a specific vehicle model    | DELETE | http://localhost:8100/api/models/:id/              |
| List automobiles                   | GET    | http://localhost:8100/api/automobiles/             |
| Create an automobile               | POST   | http://localhost:8100/api/automobiles/             |
| Get a specific automobile          | GET    | http://localhost:8100/api/automobiles/:vin/        |
| Update a specific automobile       | PUT    | http://localhost:8100/api/automobiles/:vin/        |
| Delete a specific automobile       | DELETE | http://localhost:8100/api/automobiles/:vin/        |
| List technicians                   | GET    | http://localhost:8080/api/technicians/             |
| Create a technician                | POST   | http://localhost:8080/api/technicians/             |
| Delete a specific technician       | DELETE | http://localhost:8080/api/technicians/:id/         |
| List appointments                  | GET    | http://localhost:8080/api/appointments/            |
| Create an appointment              | POST   | http://localhost:8080/api/appointments/            |
| Delete an appointment              | DELETE | http://localhost:8080/api/appointments/:id/        |
| Set appt status to "canceled"      | PUT    | http://localhost:8080/api/appointments/:id/cancel/ |
| Set appt status to "finished"      | PUT    | http://localhost:8080/api/appointments/:id/finish/ |
| List salespeople                   | GET    | http://localhost:8090/api/salespeople/             |
| Create a salesperson               | POST   | http://localhost:8090/api/salespeople/             |
| Delete a specific salesperson      | DELETE | http://localhost:8090/api/salespeople/:id/         |
| List customers                     | GET    | http://localhost:8090/api/customers/               |
| Create a customer                  | POST   | http://localhost:8090/api/customers/               |
| Delete a specific customer         | DELETE | http://localhost:8090/api/customers/:id/           |
| List sales                         | GET    | http://localhost:8090/api/sales/                   |
| Create a sale                      | POST   | http://localhost:8090/api/sales/                   |
| Delete a sale                      | DELETE | http://localhost:8090/api/sales/:id                |
|--------------------------------------------------------------------------------------------------|


## How to make JSON requests

Create and update a vehicle model (SEND THIS JSON BODY):

<!-- {
"name": "Sebring",
"picture_url": "image.yourpictureurl.com"
"manufacturer_id": 1
} -->

The return value of creating, viewing, updating a single manufacturer:

<!-- {
	"href": "/api/manufacturers/3/",
	"id": 3,
	"name": "Tesla"
} -->

Getting a list of manufacturers return value:

<!-- {
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
} -->

Create and update a vehicle model (SEND THIS JSON BODY):

<!--
{
  "name": "CyberTruck",
  "picture_url": "https://i.insider.com/6596b7dfec62ab5daf809002?width=700",
  "manufacturer_id": 3
} -->


Updating a vehicle model can take the name and/or picture URL:

<!-- {
  "name": "CyberTruck",
  "picture_url": "https://www.topgear.com/sites/default/files/cars-car/image/2023/11/1-Tesla-Cybertruck-review.jpg,
} -->

Return value of creating or updating a vehicle model:
- This returns the manufacturer's information as well

<!-- {
	"href": "/api/models/5/",
	"id": 5,
	"name": "CyberTruck",
	"picture_url": "https://www.topgear.com/sites/default/files/cars-car/image/2023/11/1-Tesla-Cybertruck-review.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/3/",
		"id": 3,
		"name": "Tesla"
	}
} -->

Getting a List of Vehicle Models Return Value:

<!-- {
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Corolla",
			"picture_url": "https://cars.usnews.com/static/images/Auto/izmo/i38431320/2018_toyota_corolla_angularfront.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Toyota"
			}
		}, -->

Create an automobile (SEND THIS JSON BODY):
- You cannot make two automobiles with the same vin

<!-- {
  "color": "black",
  "year": 2023,
  "vin": "4T1SV21E0KD098227",
  "model_id": 3
} -->

Return Value of Creating an Automobile:

<!-- {
			"href": "/api/automobiles/4T1SV21E0KD098227/",
			"id": 2,
			"color": "black",
			"year": 2023,
			"vin": "4T1SV21E0KD098227",
			"model": {
				"href": "/api/models/3/",
				"id": 3,
				"name": "Camry",
				"picture_url": "",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Toyota"
				} -->

To get the details of a specific automobile, you can query by its VIN:
example url: http://localhost:8100/api/automobiles/4T1SV21E0KD098227/

Return Value:

<!-- {
	"href": "/api/automobiles/4T1SV21E0KD098227/",
	"id": 2,
	"color": "black",
	"year": 2023,
	"vin": "4T1SV21E0KD098227",
	"model": {
		"href": "/api/models/3/",
		"id": 3,
		"name": "Camry",
		"picture_url": "",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Toyota"
		}
	}, -->

You can update the color and/or year of an automobile (SEND THIS JSON BODY):

<!--
{
  "color": "purple",
  "year": 2016
} -->

Getting a list of Automobile Return Value:

<!-- {
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN120174/",
			"id": 1,
			"color": "silver",
			"year": 2023,
			"vin": "1C3CC5FB2AN120174",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Corolla",
				"picture_url": "https://cars.usnews.com/static/images/Auto/izmo/i38431320/2018_toyota_corolla_angularfront.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Toyota"
				}
			}, -->

To create a Customer (SEND THIS JSON BODY):

<!-- {
  "first_name": "testfeb8",
  "last_name": "testfeb8",
  "address": "123 this street, that state, TS 12345",
	"phone_number": "0000000028"
} -->

Return Value of Creating a Customer:

<!-- {
	"first_name": "testfeb8",
	"last_name": "testfeb8",
	"address": "123 this street, that state, TS 12345",
	"phone_number": "0000000028",
	"id": 9
} -->

Return value of Listing all Customers:

<!-- {
	"customer": [
		{
			"first_name": "customer1",
			"last_name": "customer1",
			"address": "customer1",
			"phone_number": "00001",
			"id": 1
		},
		{
			"first_name": "customer3",
			"last_name": "customer3",
			"address": "customer3",
			"phone_number": "00003",
			"id": 3
		}, -->

To create a salesperson (SEND THIS JSON BODY):

<!--
{
  "first_name": "testsales28",
  "last_name": "testsales28",
  "employee_id": "testsales28"
} -->

Return Value of creating a salesperson:

<!-- {
	"first_name": "testsales28",
	"last_name": "testsales28",
	"employee_id": "testsales28",
	"id": 19
} -->

List all salespeople Return Value:

<!-- {
	"salespersons": [
		{
			"first_name": "test4",
			"last_name": "test4",
			"employee_id": "0004",
			"id": 5
		},
    {
			"first_name": "testsales28",
			"last_name": "testsales28",
			"employee_id": "testsales28",
			"id": 19
		}
	]
} -->

List all Salesrecords Return Value:

<!-- {
	"sale": [
		{
			"automobile": {
				"import_href": "/api/automobiles/4T1SV21E0KD098227/",
				"vin": "4T1SV21E0KD098227",
				"sold": false
			},
			"salesperson": {
				"first_name": "test4",
				"last_name": "test4",
				"employee_id": "0004",
				"id": 5
			},
			"customer": {
				"first_name": "customer3",
				"last_name": "customer3",
				"address": "customer3",
				"phone_number": "00003",
				"id": 3
			},
			"price": 2000,
			"id": 3
		},
    		{
			"automobile": {
				"import_href": "/api/automobiles/4T1SV21E0KD098227/",
				"vin": "4T1SV21E0KD098227",
				"sold": false
			},
			"salesperson": {
				"first_name": "reacttests1",
				"last_name": "reacttests1",
				"employee_id": "reacttests1",
				"id": 8
			},
			"customer": {
				"first_name": "customer3",
				"last_name": "customer3",
				"address": "customer3",
				"phone_number": "00003",
				"id": 3
			},
			"price": 5000,
			"id": 5
		}
	]
} -->

Create a New Sale (SEND THIS JSON BODY):

<!-- {
  "automobile": "4T1SV21E0KD098227",
  "salesperson": 8,
  "customer": 3,
	"price": "5000"
} -->

Return Value of Creating a New Sale:

<!-- {
	"automobile": {
		"import_href": "/api/automobiles/4T1SV21E0KD098227/",
		"vin": "4T1SV21E0KD098227",
		"sold": false
	},
	"salesperson": {
		"first_name": "reacttests1",
		"last_name": "reacttests1",
		"employee_id": "reacttests1",
		"id": 8
	},
	"customer": {
		"first_name": "customer3",
		"last_name": "customer3",
		"address": "customer3",
		"phone_number": "00003",
		"id": 3
	},
	"price": "5000",
	"id": 6
} -->

Showing a list of technicians will return this data:

<!-- {
	"technicians": [
		{
			"name": "Donald",
			"employee_number": 1,
			"id": 1
		}, -->

Getting the Details of a Technician:

<!-- {
	"name": "Donald",
	"employee_number": 1,
	"id": 1
} -->

Creating a technician will look like this:

<!-- {
	"first_name": "Test2",
	"last_name": "User2",
	"employee_id": "22"
} -->

Listing service appointments via a GET request to http://localhost:8080/api/appointments/ will return this:

<!-- "appointments": [
		{
			"id": 5,
			"date_time": "2024-02-16T10:24:00+00:00",
			"reason": "Not Our Car",
			"status": "Scheduled",
			"vin": "2222222222222",
			"customer": "New Guy",
			"technician": {
				"id": 1,
				"first_name": "Test",
				"last_name": "User",
				"employee_id": "123"
			}
		},
        {
			"id": 7,
			"date_time": "2024-02-10T08:39:00+00:00",
			"reason": "Merge Test",
			"status": "Finished",
			"vin": "6666666666",
			"customer": "Merge Test",
			"technician": {
				"id": 6,
				"first_name": "Merge",
				"last_name": "Test",
				"employee_id": "02"
			}
		}
	]
} -->

Creating a service appointment is done like the following:

<!-- {
            "date_time": dateTime,
            "reason": reason,
            "vin": vin,
            "status": 'Scheduled',
            "customer": customer,
            "technician": technician,
        }; -->
