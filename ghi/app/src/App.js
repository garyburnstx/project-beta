import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianList from './components/Service/TechnicianList';
import TechnicianForm from './components/Service/TechnicianForm';
import AppointmentList from './components/Appointments/AppointmentList';
import AppointmentForm from './components/Appointments/AppointmentForm';
import ServiceHistory from './components/Service/ServiceHistory';
import AutomobileForm from './components/Automobiles/AutomobileForm';
import AutomobileInventory from './components/Automobiles/AutomobileInventory';
import VehicleModelForm from './components/Vehicles/VehicleModelForm';
import CustomersList from "./components/Customers/CustomersList";
import CustomersForm from "./components/Customers/CustomersForm";
import SalesList from "./components/Sales/SalesList";
import SalesForm from "./components/Sales/SalesForm";
import SalespersonList from "./components/Sales/SalespersonList";
import SalespersonForm from "./components/Sales/SalespersonForm";
import SalespersonHistory from "./components/Sales/SalespersonHistory";
import ManufacturerForm from "./components/Manufacturer/ManufacturerForm";
import ManufacturerList from "./components/Manufacturer/ManufacturerList";
import ModelList from "./components/Vehicles/ModelList";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/servicehistory" element={<ServiceHistory />} />
        <Route path="/" element={<MainPage />} />

        {/* Path for salesperson */}
        <Route path="salespeople" element={<SalespersonList />} />
        <Route path="salespeople/create" element={<SalespersonForm />} />
        <Route path="salespeople/history" element={<SalespersonHistory />} />

        {/* Path for customer */}
        <Route path="customers" element={<CustomersList />} />
        <Route path="customers/create" element={<CustomersForm />} />

        {/* Path for Automobile */}
        <Route path="/AutomobileForm" element={<AutomobileForm />} />
        <Route path="/AutomobileInventory" element={<AutomobileInventory />} />

        {/* Path for sale */}
        <Route path="sales" element={<SalesList />} />
        <Route path="sales/create" element={<SalesForm />} />

        {/* Path for Manufacturer */}
        <Route path="manufacturers" element={<ManufacturerList />} />
        <Route path="manufacturers/create" element={<ManufacturerForm />} />

        {/* Path for Appointment */}
        <Route path="/appointments" element={<AppointmentList />} />
        <Route path="/appointments/new" element={<AppointmentForm />} />

        {/* Path for Technician */}
        <Route path="/technicians" element={<TechnicianList />} />
        <Route path="/technicians/new" element={<TechnicianForm />} />

        {/* Path for Vehicle */}
        <Route path="vehicles" element={<ModelList />} />
        <Route path="vehicles/create" element={<VehicleModelForm />} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
