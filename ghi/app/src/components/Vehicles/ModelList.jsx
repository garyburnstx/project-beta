import { useState, useEffect} from 'react';

function ModelList() {
    const [VehicleModel, setVehicleModel] = useState([])

    const getData = async ()=> {
    const response = await fetch('http://localhost:8100/api/models/');
    if (response.ok) {
        const VehicleModel = await response.json();
        setVehicleModel(VehicleModel.models);
        console.log(VehicleModel)
    } else {
        console.error('An error occurred fetching the data')
    }
}

    useEffect(()=> {
        getData()
    }, []);

    const handleDelete = async (event) => {
        event.preventDefault();
        const id = event.target.value;

        const url = `http://localhost:8100/api/models/${id}/`;
        const fetchOptions = {
            method: "DELETE",
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const Response = await fetch(url, fetchOptions);
        if (Response.ok) {
            alert("Vehicle model DELETED!");
            console.log("Vehicle model DELETED!");
            getData();
        } else {
            alert("Vehicle model could not be deleted!")
            console.log("Vehicle model could not be deleted!")
        }
    }

    return (
        <div className="my-5 container">
        <div className="row">
            <h1>Vehicle Models List</h1>

            <table className="table table-striped m-3">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {VehicleModel?.map(model => {
                    return (
                        <tr key={model.id}>
                            <td className="align-middle">{ model.name }</td>
                            <td className="align-middle">{ model.manufacturer.name }</td>
                            <td>
                                <img src={ model.picture_url } className="img-thumbnail" width="150"></img>
                            </td>
                            <td className="align-middle">
                                <button type="button"
                                value={model.id}
                                className="btn btn-danger"
                                onClick={handleDelete}>
                                    Delete</button>
                            </td>
                        </tr>
                    );
                    })}
            </tbody>
            </table>
        </div>
        </div>
    );
}

export default ModelList;
