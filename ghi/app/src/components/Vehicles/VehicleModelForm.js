import React, { useState, useEffect } from 'react';

function VehicleModelForm() {
    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturer, setManufacturer] = useState(0);
    const [manufacturers, setManufacturers] = useState([]);

    useEffect(() => {
        const fetchManufacturers = async () => {
            const response = await fetch('http://localhost:8100/api/manufacturers/');
            const data = await response.json();
            setManufacturers(data.manufacturers);
        };

        fetchManufacturers();
    }, []);

    const handleNameChange = (e) => {
        setName(e.target.value);
    };

    const handlePictureUrlChange = (e) => {
        setPictureUrl(e.target.value);
    };

    const handleManufacturerChange = (e) => {
        setManufacturer(e.target.value);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {
            name: name,
            picture_url: pictureUrl,
            manufacturer_id: parseInt(manufacturer)
        };

        const url = 'http://localhost:8100/api/models/';
        const fetchOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        };

        const response = await fetch(url, fetchOptions);

        if (response.ok) {
            alert('Vehicle model was added successfully!');
            setName('');
            setPictureUrl('');
            setManufacturer('');
        } else {
            alert('Failed to add vehicle model!');
        }
    };

    return (
        <form onSubmit={handleSubmit} className="text-center">
            <h2>Add a vehicle model</h2>
            <div className="form-group">
                <label>
                    Model Name:
                    <input type="text" name="name" value={name} onChange={handleNameChange} className="form-control" />
                </label>
            </div>
            <div className="form-group">
                <label>
                    Picture URL:
                    <input type="text" name="pictureUrl" value={pictureUrl} onChange={handlePictureUrlChange} className="form-control" />
                </label>
            </div>
            <div className="form-group">
                Preview:
                <div>
                {pictureUrl && <img src={pictureUrl} style={{maxWidth: '100%', maxHeight: '300px'}} />}
                </div>
            </div>
            <div className="form-group">
                <label>
                    Manufacturer:
                    <select name="manufacturer" value={manufacturer} onChange={handleManufacturerChange} className="form-control select-arrow" required>
                        <option value="">Choose a manufacturer</option>
                        {manufacturers.map(manufacturer => (
                            <option key={manufacturer.id} value={manufacturer.id}>
                                {manufacturer.name}
                            </option>
                        ))}
                    </select>
                </label>
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
    );
}

export default VehicleModelForm;
