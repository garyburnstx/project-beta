import { useState, useEffect} from 'react';

function CustomerList() {
    const [customer, setCustomer] = useState([])

    const getData = async ()=> {
    const response = await fetch('http://localhost:8090/api/customers/');
    if (response.ok) {
        const { customer } = await response.json();
        console.log(customer)
        setCustomer(customer);
    } else {
        console.error('An error occurred fetching the data')
    }
}

    useEffect(()=> {
        getData()
    }, []);

    const handleDelete = async (event) => {
        event.preventDefault();
        const id = event.target.value;

        const url = `http://localhost:8090/api/customers/${id}/`;
        const fetchOptions = {
            method: "DELETE",
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const Response = await fetch(url, fetchOptions);
        if (Response.ok) {
            alert("Customer DELETED!");
            console.log("Customer DELETED!");
            getData();
        } else {
            alert("Customer could not be deleted!")
            console.log("Customer could not be deleted!")
        }
    }

    return (
        <div className="my-5 container">
        <div className="row">
            <h1>Customers</h1>

            <table className="table table-striped m-3">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                </tr>
            </thead>
            <tbody>
                {customer?.map(customer => {
                    return (
                        <tr key={customer.id}>
                            <td className="align-middle">{ customer.first_name }</td>
                            <td className="align-middle">{ customer.last_name }</td>
                            <td className="align-middle">{ customer.address }</td>
                            <td className="align-middle">{ customer.phone_number }</td>
                            <td className="align-middle">
                                <button type="button"
                                value={customer.id}
                                className="btn btn-danger"
                                onClick={handleDelete}>
                                    Delete</button>
                            </td>
                        </tr>
                    );
                    })}
            </tbody>
            </table>
        </div>
        </div>
    );
}

export default CustomerList;
