import React, { useEffect, useState } from 'react';

function SaleForm( ) {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phone_number, setPhoneNumber] = useState('');

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }

    const handlePhoneNumberChange = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8090/api/customers/';

        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.address = address;
        data.phone_number = phone_number;

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
            };
            alert("Customer CREATED!");
        }


    // const fetchData = async () => {
    //     const url = 'http://localhost:8100/api/bins/';

    //     const response = await fetch(url);

    //     if (response.ok) {
    //     const data = await response.json();
    //     setBins(data.bins);
    //     }
    // }

    // useEffect(() => {
    //     fetchData();
    // }, []);


    //   const handleFormChange = (e) => {
    //     const value = e.target.value;
    //     const inputName = e.target.name;

    //     setFormData({
    //       ...formData,
    //       [inputName]: value
    //     });
    //   }
            return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create new Customer</h1>
                <form onSubmit={handleSubmit} id="create-customer-form">

                <div className="form-floating mb-3">
                    <input onChange={handleFirstNameChange} value={first_name} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                    <label htmlFor="first_name">First Name</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleLastNameChange} value={last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                    <label htmlFor="last_name">Last Name</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleAddressChange} value={address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="address">Address</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handlePhoneNumberChange} value={phone_number} placeholder="Phone Number" required type="number" name="phone_number" id="phone_number" className="form-control" />
                    <label htmlFor="phone_number">Phone Number</label>
                </div>

                    {/* <div className="mb-3">
                    <select onChange={handleFormChange} value={formData.bin} required name="bin" id="bin" className="form-select">
                        <option value="">Choose a Bin</option>
                        {bins.map(bin => {
                        return (
                            <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                        )
                        })}
                    </select>
                    </div> */}

                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
            </div>
        )
    }

export default SaleForm;
