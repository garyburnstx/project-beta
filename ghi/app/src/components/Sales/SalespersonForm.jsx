import React, { useEffect, useState } from 'react';

function SalespersonForm( ) {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeId] = useState('');

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8090/api/salespeople/';

        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeId('');
            };
            alert("Salesperson CREATED!");
        }

    // useEffect(() => {
    //     fetchData();
    // }, []);


    //   const handleFormChange = (e) => {
    //     const value = e.target.value;
    //     const inputName = e.target.name;

    //     setFormData({
    //       ...formData,
    //       [inputName]: value
    //     });
    //   }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add a New Salesperson</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form">

            <div className="form-floating mb-3">
                <input onChange={handleFirstNameChange} value={first_name} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                <label htmlFor="first_name">First Name</label>
            </div>

            <div className="form-floating mb-3">
                <input onChange={handleLastNameChange} value={last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                <label htmlFor="last_name">Last Name</label>
            </div>

            <div className="form-floating mb-3">
                <input onChange={handleEmployeeIdChange} value={employee_id} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" />
                <label htmlFor="employee_id">Employee ID</label>
            </div>


            {/* <div className="mb-3">
                <select onChange={handleFormChange} value={formData.bin} required name="bin" id="bin" className="form-select">
                    <option value="">Choose a Bin</option>
                    {bins.map(bin => {
                    return (
                        <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                    )
                    })}
                </select>
            </div> */}

                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    )
}

export default SalespersonForm;
