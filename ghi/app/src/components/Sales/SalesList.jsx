import { useState, useEffect} from 'react';

function SaleList() {
    const [sale, setSale] = useState([])

    const getData = async ()=> {
    const response = await fetch('http://localhost:8090/api/sales/');
    if (response.ok) {
        const { sale } = await response.json();
        console.log(sale)
        setSale(sale);
    } else {
        console.error('An error occurred fetching the data')
    }
}

    useEffect(()=> {
        getData()
    }, []);

    const handleDelete = async (event) => {
        event.preventDefault();
        const id = event.target.value;

        const url = `http://localhost:8090/api/customers/${id}/`;
        const fetchOptions = {
            method: "DELETE",
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const Response = await fetch(url, fetchOptions);
        if (Response.ok) {
            alert("Sale DELETED!");
            console.log("Sale DELETED!");
            getData();
        } else {
            alert("Sale could not be deleted!")
            console.log("Sale could not be deleted!")
        }
    }

    return (
        <div className="my-5 container">
        <div className="row">
            <h1>Sales</h1>

            <table className="table table-striped m-3">
            <thead>
                <tr>
                <th>Salesperson Employee ID</th>
                <th>Salesperson Name</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sale?.map(sale => {
                return (
                    <tr key={sale.id}>
                    <td className="align-middle">{ sale.salesperson.employee_id }</td>
                    <td className="align-middle">{ sale.salesperson.first_name + " " + sale.salesperson.last_name }</td>
                    <td className="align-middle">{ sale.customer.first_name + " " + sale.customer.last_name }</td>
                    <td className="align-middle">{ sale.automobile.vin }</td>
                    <td className="align-middle">{ sale.price }</td>
                    <td className="align-middle">
                        <button type="button"
                        value={sale.id}
                        className="btn btn-danger"
                        onClick={handleDelete}>
                            Delete</button>
                    </td>
                    </tr>
                );
                })}
            </tbody>
            </table>
        </div>
        </div>
    );
}

export default SaleList;
