import { useState, useEffect} from 'react';

function SalespersonList() {
    const [salesperson, setSalesperson] = useState([])

    const getData = async ()=> {
    const response = await fetch('http://localhost:8090/api/salespeople/');
    if (response.ok) {
        const salesperson  = await response.json();
        setSalesperson(salesperson.salespersons);
    } else {
        console.error('An error occurred fetching the data')
    }
}

    useEffect(()=> {
        getData()
    }, []);

    const handleDelete = async (event) => {
        event.preventDefault();
        const id = event.target.value;

        const url = `http://localhost:8090/api/salespeople/${id}/`;
        const fetchOptions = {
            method: "DELETE",
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const Response = await fetch(url, fetchOptions);
        if (Response.ok) {
            alert("Salesperson DELETED!");
            console.log("Salesperson DELETED!");
            getData();
        } else {
            alert("Salesperson could not be deleted!")
            console.log("Salesperson could not be deleted!")
        }
    }

    return (
        <div className="my-5 container">
        <div className="row">
            <h1>Salespeople</h1>

            <table className="table table-striped m-3">
                <thead>
                    <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salesperson?.map(salesperson => {
                    return (
                        <tr key={salesperson.id}>
                        <td className="align-middle">{ salesperson.employee_id }</td>
                        <td className="align-middle">{ salesperson.first_name }</td>
                        <td className="align-middle">{ salesperson.last_name }</td>
                        <td className="align-middle">
                            <button type="button"
                            value={salesperson.id}
                            className="btn btn-danger"
                            onClick={handleDelete}>
                                Delete</button>
                        </td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
        </div>
    );
}

export default SalespersonList;
