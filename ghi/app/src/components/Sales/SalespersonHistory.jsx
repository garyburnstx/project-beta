import React, { useEffect, useState } from 'react';

function SalespersonHistoryForm() {

    const [salespeople, setSalespeople] = useState([]);
    const [selectedsalesperson, setSelectedSalespeople] = useState([]);
    const [sales, setSales] = useState([]);

    // const handleFilteredSalespeopleChange = (event) => {
    //     const value = event.target.value;
    //     const filtered = salespeople.filter(salespeople)
    // }

    const handleSalespeopleChange = (event) => {
        const value = event.target.value;
        setSelectedSalespeople(value);
        console.log(selectedsalesperson)
    }

    const fetchData = async () => {
        const salespeopleResponse = await fetch('http://localhost:8090/api/salespeople');
        if (salespeopleResponse.ok) {
            const data = await salespeopleResponse.json();
            setSalespeople(data.salespersons);
        };
        const saleResponse = await fetch('http://localhost:8090/api/sales/');
        if (saleResponse.ok) {
            const data = await saleResponse.json();
            setSales(data.sale);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const filteredSale = sales.filter(s => s.salesperson.id === parseInt(selectedsalesperson));
    console.log("filteredSale", filteredSale)
    console.log("sales", sales)
    console.log(typeof selectedsalesperson)

    // const filteredSalespeople = (event) => {
    //     const value = event.target.value;
    //     const filtered = salespeople.filter(salespeople => salespeople.id.includes(sale));
    //     setFilteredUsers(filtered);
    //   };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Salesperson History</h1>
                    <div className="mb-3">
                        <select value={selectedsalesperson} onChange={handleSalespeopleChange} className="form-select" id="salespeople">
                            <option value="">Choose a Salesperson</option>
                            {salespeople.map(salesperson => {
                                return (
                                    <option key={salesperson.id} value={salesperson.id}>
                                        {salesperson.first_name + " " + salesperson.last_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <table className="table table-striped m-3">
                        <thead>
                            <tr>
                                <th>Salesperson</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredSale.map(s => {
                                return (
                                    <tr key={s.id}>
                                        <td className="align-middle">{s.salesperson.first_name + " " + s.salesperson.last_name}</td>
                                        <td className="align-middle">{s.customer.first_name + " " + s.customer.last_name}</td>
                                        <td className="align-middle">{s.automobile.vin}</td>
                                        <td className="align-middle">{s.price}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default SalespersonHistoryForm;
