import React, { useEffect, useState } from 'react';

function SaleForm( ) {
    const [price, setPrice] = useState('');
    const [automobile, setAutomobile] = useState('');
    const [salesperson, setSalesperson] = useState('');
    const [customer, setCustomer] = useState('');
    const [automobiles, setAutomobiles] = useState([]);
    const [salespersons, setSalespersons] = useState([]);
    const [customers, setCustomers] = useState([]);

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    const handleAutomobileNameChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8090/api/sales/';

        const data = {};
        data.automobile = automobile;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setPrice('');
            setAutomobile('');
            setCustomer('');
            setSalesperson('');
            };
            alert("Sale CREATED!");
        }

    const fetchData = async () => {
        const automobilesResponse = await fetch('http://localhost:8100/api/automobiles/');
        if (automobilesResponse.ok) {
            const automobilesData = await automobilesResponse.json();
            setAutomobiles(automobilesData.autos);
        }
        const salespersonsResponse = await fetch('http://localhost:8090/api/salespeople');
        if (salespersonsResponse.ok) {
            const salespersonsdata = await salespersonsResponse.json();
            setSalespersons(salespersonsdata.salespersons);
        }
        const customersResponse = await fetch('http://localhost:8090/api/customers');
        if (customersResponse.ok) {
            const customersdata = await customersResponse.json();
            setCustomers(customersdata.customer);
        };
    }
    useEffect(() => {
        fetchData();
    }, []);


    //   const handleFormChange = (e) => {
    //     const value = e.target.value;
    //     const inputName = e.target.name;

    //     setFormData({
    //       ...formData,
    //       [inputName]: value
    //     });
    //   }

    const filteredAutos = automobiles.filter(automobile => automobile.sold === false)

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a New Sale</h1>
            <form onSubmit={handleSubmit} id="create-sales-form">

            <div className="form-floating mb-3">
                <select onChange={handleAutomobileNameChange} value={automobile} required name="automobile" id="automobile" className="form-select">
                <option value="">Choose an Automobile</option>
                {filteredAutos.map(automobile => {
                    return (
                        <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                    )
                    })}
                </select>
            </div>

            <div className="form-floating mb-3">
                <select onChange={handleCustomerChange} value={customer} placeholder="Customer" required name="customer" id="customer" className="form-select">
                <option value="">Choose a Customer</option>
                {customers.map(customer => {
                    return (
                        <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                    )
                    })}
                </select>
            </div>

            <div className="form-floating mb-3">
                <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select" >
                <option value="">Choose a Salesperson</option>
                {salespersons.map(salesperson => {
                    return (
                        <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                    )
                    })}
                </select>
            </div>

            <div className="form-floating mb-3">
                <input onChange={handlePriceChange} value={price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                <label htmlFor="price">Price</label>
            </div>

            {/* <div className="mb-3">
                <select onChange={handleFormChange} value={formData.bin} required name="bin" id="bin" className="form-select">
                    <option value="">Choose a Bin</option>
                    {bins.map(bin => {
                    return (
                        <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                    )
                    })}
                </select>
            </div> */}

                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    )
}

export default SaleForm;
