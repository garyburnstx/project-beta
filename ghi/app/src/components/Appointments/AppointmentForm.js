import React, { useState, useEffect } from 'react';
import { Navigate } from 'react-router-dom';

function CreateAppointmentForm() {
    const [dateTime, setDateTime] = useState('');
    const [reason, setReason] = useState('');
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [technician, setTechnician] = useState('');
    const [technicianList, setTechnicianList] = useState([]);
    const [isSubmitted, setIsSubmitted] = useState(false);

    useEffect(() => {
        const fetchTechnicians = async () => {
            const response = await fetch('http://localhost:8080/api/technicians/');
            const data = await response.json();
            setTechnicianList(data.technicians);
        };

        fetchTechnicians();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            date_time: dateTime,
            reason: reason,
            vin: vin,
            status: 'Scheduled',
            customer: customer,
            technician: technician,
        };

        const url = 'http://localhost:8080/api/appointments/';
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchOptions);
        if (response.ok) {
            setDateTime('');
            setReason('');
            setVin('');
            setCustomer('');
            setTechnician('');
            setIsSubmitted(true);
        }
    }

    if (isSubmitted) {
        return <Navigate to="/appointments" />;
    }

    return (
        <form onSubmit={handleSubmit} className="text-center">
            <div className="form-group">
                <label>
                    Date and Time:
                    <input type="datetime-local" name="date_time" value={dateTime} onChange={e => setDateTime(e.target.value)} className="form-control" />
                </label>
            </div>
            <div className="form-group">
                <label>
                    Reason:
                    <input type="text" name="reason" value={reason} onChange={e => setReason(e.target.value)} className="form-control" />
                </label>
            </div>
            <div className="form-group">
                <label>
                    VIN:
                    <input type="text" name="vin" value={vin} onChange={e => setVin(e.target.value)} className="form-control" />
                </label>
            </div>
            <div className="form-group">
                <label>
                    Customer:
                    <input type="text" name="customer" value={customer} onChange={e => setCustomer(e.target.value)} className="form-control" />
                </label>
            </div>
            <div className="form-group">
                <label>
                    Technician:
                    <select name="technician" value={technician} onChange={e => setTechnician(e.target.value)} className="form-control select-arrow" required>
                        <option value="">Choose a technician</option>
                        {technicianList.map(technician => (
                            <option key={technician.id} value={technician.id}>
                                {technician.first_name} {technician.last_name}
                            </option>
                        ))}
                    </select>
                </label>
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
    );
};

export default CreateAppointmentForm;
