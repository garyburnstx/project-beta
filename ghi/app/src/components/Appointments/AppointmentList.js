import React, { useState, useEffect } from 'react';

function AppointmentList() {
    const [appointments, setAppointments] = useState([]);
    const [sold, setSold] = useState([]);

    const fetchAppointments = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        const data = await response.json();
        setAppointments(data.appointments);
    };


    useEffect(() => {

        const fetchSold = async () => {
            const response = await fetch('http://localhost:8100/api/automobiles/');
            const data = await response.json();
            console.log(data);
            setSold(data.autos);
        };

        fetchAppointments();
        fetchSold();
    }, []);

    const setAsCancelled = async (appointmentId) => {
        await fetch(`http://localhost:8080/api/appointments/${appointmentId}/cancel/`, {
            method: 'PUT'
        });
        fetchAppointments();
    };

    const setAsFinished = async (appointmentId) => {
        await fetch(`http://localhost:8080/api/appointments/${appointmentId}/finish/`, {
            method: 'PUT'
        });
        fetchAppointments();
    };

    return (
        <div>
            <h2>Appointments</h2>
            {appointments.filter(appointment => appointment.status === 'Scheduled').map(appointment => {
                const date = new Date(appointment.date_time);
                const formattedDate = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
                const isVIP = sold.some(auto => auto.vin === appointment.vin && auto.sold === true);
                console.log(sold)

                return (
                    <div key={appointment.id}>
                        <hr />
                        <h3>{appointment.reason}</h3>
                        <p><strong>Status:</strong> {appointment.status}</p>
                        <p><strong>Date:</strong> {formattedDate}</p>
                        <p><strong>VIN:</strong> {appointment.vin} {isVIP && <strong>(VIP)</strong>}</p>
                        <p><strong>Customer:</strong> {appointment.customer}</p>
                        <p><strong>Technician:</strong> {appointment.technician.first_name} {appointment.technician.last_name}</p>
                        <button onClick={() => setAsCancelled(appointment.id)}>
                            Mark as Cancelled
                        </button>
                        <button onClick={() => setAsFinished(appointment.id, 'Finished')}>
                            Mark as Finished
                        </button>
                        <hr />
                    </div>
                );
            })}
        </div>
    );
}

export default AppointmentList;
