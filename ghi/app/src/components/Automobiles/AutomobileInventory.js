import React, { useState, useEffect } from 'react';

function AutomobileInventory() {
    const [automobiles, setAutomobiles] = useState([]);

    const fetchAutomobiles = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        const data = await response.json();
        console.log(data);
        setAutomobiles(data.autos);
    };

    useEffect(() => {
        const fetchData = async () => {
            const data = await fetchAutomobiles();
            setAutomobiles(data.autos);
        };

        fetchData();
    }, []);

    return (
        <div>
            <h2>Automobile Inventory</h2>
            <table className="table table-dark table-striped table-hover table-bordered">
                <thead className="table-light">
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Manufacturer</th>
                        <th>Model</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map(auto => (
                        <tr key={auto.vin}>
                            <td>{auto.vin}</td>
                            <td style={{color: auto.color}}><strong>{auto.color}</strong></td>
                            <td>{auto.year}</td>
                            <td>{auto.model.manufacturer.name}</td>
                            <td>{auto.model.name}</td>
                            <td style={{fontSize: "1.5rem", color: auto.sold ? 'green' : 'red'}}><strong>{auto.sold ? <>&#x2713;</> : <>X</>}</strong></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default AutomobileInventory;
