import React, { useState, useEffect } from 'react';

function AutomobileForm() {
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [model, setModel] = useState(0);
    const [models, setModels] = useState([]);

    useEffect(() => {
        const fetchModels = async () => {
            const response = await fetch('http://localhost:8100/api/models/');
            const data = await response.json();
            setModels(data.models);
        };

        fetchModels();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {
            color: color,
            year: parseInt(year, 10),
            vin: vin,
            model_id: parseInt(model, 10)
        };

        const url = 'http://localhost:8100/api/automobiles/';
        console.log(JSON.stringify(data))
        const fetchOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        };

        const response = await fetch(url, fetchOptions);
        console.log(response)

        if (response.ok) {
            alert('Automobile was added successfully!');
            setColor('');
            setYear('');
            setVin('');
            setModel('');
        } else {
            alert('Failed to add automobile!');
        }
    };

    return (
        <form onSubmit={handleSubmit} className="text-center">
            <h2>Add an automobile to inventory</h2>
            <div className="form-group">
                <label>
                    Color:
                    <input type="text" name="color" value={color} onChange={e => setColor(e.target.value)} className="form-control" />
                </label>
            </div>
            <div className="form-group">
                <label>
                    Year:
                    <input type="text" name="year" value={year} onChange={e => setYear(e.target.value)} className="form-control" />
                </label>
            </div>
            <div className="form-group">
                <label>
                    VIN:
                    <input type="text" name="vin" value={vin} onChange={e => setVin(e.target.value)} className="form-control" />
                </label>
            </div>
            <div className="form-group">
                <label>
                    Vehicle Model:
                    <select name="model" value={model} onChange={e => setModel(e.target.value)} className="form-control select-arrow" required>
                        <option value="">Choose a model</option>
                        {models.map(model => (
                            <option key={model.id} value={model.id}>
                                {model.manufacturer.name} {model.name}
                            </option>
                        ))}
                    </select>
                </label>
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
    );
}

export default AutomobileForm;
