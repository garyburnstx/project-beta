import React, { useEffect, useState } from 'react'

const TechnicianList = () => {
    const [technicianList, setTechnicianList] = useState([])

    useEffect(() => {
        fetchTechnicianList()
    }, [])

    const fetchTechnicianList = async () => {
        try {
            const response = await fetch('http://localhost:8080/api/technicians/');
            const data = await response.json();
            setTechnicianList(data.technicians);
        } catch (error) {
            console.error("Technicians aren't populating! Here's what I got:", error);
        }
    }

    const deleteTechnician = async (technicianId) => {
        await fetch(`http://localhost:8080/api/technicians/${technicianId}`, {
            method: 'DELETE',
        });
        fetchTechnicianList();
    }
    console.log(technicianList)
    return (
        <div className="container">
            <h1 className="text-center">Technician List</h1>
            {technicianList.length > 0 ? (
                <table className="table table-dark table-striped table-hover table-bordered">
                    <thead className="table-light">
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Employee ID</th>
                            <th>Delete?</th>
                        </tr>
                    </thead>
                    <tbody>
                        {technicianList.map((technician) => (
                            <tr key={technician.id}>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                                <td>{technician.employee_id}</td>
                                <td>
                                    <button className="btn btn-danger" onClick={() => deleteTechnician(technician.id)}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            ) : (
                <p>No technicians available!</p>
            )}
        </div>
    )
}

export default TechnicianList
