import React, { useState } from 'react';
import { Navigate } from 'react-router-dom';

function CreateTechnicianForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');
    const [isSubmitted, setIsSubmitted] = useState(false);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            first_name: firstName,
            last_name: lastName,
            employee_id: employeeId,
        };

        const url = 'http://localhost:8080/api/technicians/';
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchOptions);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeId('');
            setIsSubmitted(true);
        }
    }

    if (isSubmitted) {
        return <Navigate to="/technicians" />;
    }

    return (
        <form onSubmit={handleSubmit} className="text-center">
            <div className="form-group">
                <label>
                    First Name:
                    <input type="text" name="first_name" value={firstName} onChange={e => setFirstName(e.target.value)} className="form-control" />
                </label>
            </div>
            <div className="form-group">
                <label>
                    Last Name:
                    <input type="text" name="last_name" value={lastName} onChange={e => setLastName(e.target.value)} className="form-control" />
                </label>
            </div>
            <div className="form-group">
                <label>
                    Employee ID:
                    <input type="text" name="employee_id" value={employeeId} onChange={e => setEmployeeId(e.target.value)} className="form-control" />
                </label>
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
    );
};

export default CreateTechnicianForm;
