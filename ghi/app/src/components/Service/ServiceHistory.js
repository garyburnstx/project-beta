import React, { useState, useEffect } from 'react';

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);
    const [search, setSearch] = useState('');

    const fetchAppointments = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        const data = await response.json();
        setAppointments(data.appointments);
    };

    const fetchAutomobiles = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        const data = await response.json();
        console.log(data);
        setAutomobiles(data.autos);
    };

    useEffect(() => {
        fetchAppointments();
        fetchAutomobiles();
    }, []);

    const filteredAppointments = appointments.filter(appointment => appointment.vin.includes(search));

    return (
        <div>
            <h2 style={{ textAlign: 'center'}}>Service History</h2>
            <form style={{ textAlign: 'center'}} onSubmit={e => e.preventDefault()}>
                <input
                    type="text"
                    value={search}
                    onChange={e => setSearch(e.target.value)}
                    placeholder="Enter VIN"
                />
                <button type="submit">Search</button>
            </form>

            <table className="table table-dark table-striped table-hover table-bordered">
                <thead className="table-light">
                    <tr>
                        <th style={{ padding: '10px' }}>VIN</th>
                        <th style={{ padding: '10px' }}>VIP</th>
                        <th style={{ padding: '10px' }}>Customer</th>
                        <th style={{ padding: '10px' }}>Date and Time</th>
                        <th style={{ padding: '10px' }}>Technician</th>
                        <th style={{ padding: '10px' }}>Reason</th>
                        <th style={{ padding: '10px' }}>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredAppointments.map(appointment => {
                        const date = new Date(appointment.date_time);
                        const formattedDate = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
                        const isVIP = automobiles.some(auto => auto.vin === appointment.vin);
                        console.log(isVIP);

                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td style={{ padding: '10px' }}>{isVIP ? 'VIP' : 'Regular'}</td>
                                <td style={{ padding: '10px' }}>{appointment.customer}</td>
                                <td style={{ padding: '10px' }}>{formattedDate}</td>
                                <td style={{ padding: '10px' }}>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                <td style={{ padding: '10px' }}>{appointment.reason}</td>
                                <td style={{ padding: '10px' }}>{appointment.status}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ServiceHistory;
