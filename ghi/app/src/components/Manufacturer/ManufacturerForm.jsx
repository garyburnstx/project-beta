import React, { useEffect, useState } from 'react';

function ManufacturerForm( ) {
    const [name, setName] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8100/api/manufacturers/';

        const data = {};
        data.name = name;

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setName('');
            };
            alert("Manufacturer CREATED!");
        }

    // const fetchData = async () => {
    //     const url = 'http://localhost:8100/api/bins/';

    //     const response = await fetch(url);

    //     if (response.ok) {
    //     const data = await response.json();
    //     setBins(data.bins);
    //     }
    // }

    // useEffect(() => {
    //     fetchData();
    // }, []);


    //   const handleFormChange = (e) => {
    //     const value = e.target.value;
    //     const inputName = e.target.name;

    //     setFormData({
    //       ...formData,
    //       [inputName]: value
    //     });
    //   }
            return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new Manufacturer</h1>
                <form onSubmit={handleSubmit} id="create-customer-form">

                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="Manufacturer Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Manufacturer Name</label>
                </div>

                    {/* <div className="mb-3">
                    <select onChange={handleFormChange} value={formData.bin} required name="bin" id="bin" className="form-select">
                        <option value="">Choose a Bin</option>
                        {bins.map(bin => {
                        return (
                            <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                        )
                        })}
                    </select>
                    </div> */}

                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
            </div>
        )
    }

export default ManufacturerForm;
