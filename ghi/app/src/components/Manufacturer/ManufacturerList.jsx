import { useState, useEffect} from 'react';

function ManufacturerList() {
    const [manufacturerList, setManufacturerList] = useState([])

    const getData = async ()=> {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
        const m = await response.json();
        setManufacturerList(m.manufacturers);
        console.log(m)
    } else {
        console.error('An error occurred fetching the data')
    }
}

    useEffect(()=> {
        getData()
    }, []);

    return (
        <div className="my-5 container">
        <div className="row">
            <h1>Manufacturers</h1>

            <table className="table table-striped m-3">
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                {manufacturerList?.map(manufacturer => {
                    return (
                        <tr key={manufacturer.id}>
                            <td className="align-middle">{ manufacturer.name }</td>
                        </tr>
                    );
                    })}
            </tbody>
            </table>
        </div>
        </div>
    );
}

export default ManufacturerList;
