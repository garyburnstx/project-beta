import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" activeClassName="active" exact to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="manufacturerDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Manufacturer
              </a>
              <ul className="dropdown-menu" aria-labelledby="manufacturerDropdown">
                <li><NavLink className="dropdown-item" activeClassName="active" to="/manufacturers">Manufacturer List</NavLink></li>
                <li><NavLink className="dropdown-item" activeClassName="active" to="/manufacturers/create">Create a New Manufacturer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="salespersonDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Salesperson
              </a>
              <ul className="dropdown-menu" aria-labelledby="salespersonDropdown">
                <li><NavLink className="dropdown-item" activeClassName="active" to="/salespeople">Salesperson List</NavLink></li>
                <li><NavLink className="dropdown-item" activeClassName="active" to="/salespeople/create">Add a Salesperson</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="customerDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Customer
              </a>
              <ul className="dropdown-menu" aria-labelledby="customerDropdown">
                <li><NavLink className="dropdown-item" activeClassName="active" to="/customers">Customer List</NavLink></li>
                <li><NavLink className="dropdown-item" activeClassName="active" to="/customers/create">Add a Customer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="salesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
              <ul className="dropdown-menu" aria-labelledby="salesDropdown">
                <li><NavLink className="dropdown-item" activeClassName="active" to="/sales">Sales List</NavLink></li>
                <li><NavLink className="dropdown-item" activeClassName="active" to="/sales/create">Record a New Sale</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="vehicleDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Vehicle
              </a>
              <ul className="dropdown-menu" aria-labelledby="vehicleDropdown">
                <li><NavLink className="dropdown-item" activeClassName="active" to="/vehicles">Vehicle Model List</NavLink></li>
                <li><NavLink className="dropdown-item" activeClassName="active" to="/vehicles/create">Create Vehicle Model</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="appointmentDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Appointment
              </a>
              <ul className="dropdown-menu" aria-labelledby="appointmentDropdown">
                <li><NavLink className="dropdown-item" activeClassName="active" to="/appointments">Appointment List</NavLink></li>
                <li><NavLink className="dropdown-item" activeClassName="active" to="/appointments/new">Create Appointment</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="technicianDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Technician
              </a>
              <ul className="dropdown-menu" aria-labelledby="technicianDropdown">
                <li><NavLink className="dropdown-item" activeClassName="active" to="/technicians">Technician List</NavLink></li>
                <li><NavLink className="dropdown-item" activeClassName="active" to="/technicians/new">Add a Technician</NavLink></li>
              </ul>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" activeClassName="active" to="/servicehistory">Service History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" activeClassName="active" to="/salespeople/history">Sales History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
