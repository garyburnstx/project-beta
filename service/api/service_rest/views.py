from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
import json
from .models import *
from django.views.decorators.http import require_http_methods

# Create your views here.
class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["id", "first_name", "last_name", "employee_id"]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["id", "date_time", "reason", "status", "vin", "customer", "technician"]
    encoders = {"technician": TechnicianListEncoder()}

@require_http_methods(["GET", "POST", "DELETE"])
def list_technicians(request, technician_id=None):
    if request.method == "GET":
        technicians = Technician.objects.all()
    elif request.method == "POST":
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False
        )
    else:
        try:
            if technician_id is not None:
                technician = Technician.objects.get(id=technician_id)
                technician.delete()
                response = JsonResponse({"message": "Deleted successfully"})
                response.status_code = 200
                return response
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    return JsonResponse(
        {"technicians": technicians},
        encoder=TechnicianListEncoder,
        safe=False
    )

@require_http_methods(["GET", "POST", "DELETE"])
def list_appointments(request, appointment_id=None):
    if request.method == "GET":
        appointments = Appointment.objects.all()
    elif request.method == "POST":
        content = json.loads(request.body)
        technician_id = content["technician"]
        technician = Technician.objects.get(id=technician_id)
        content["technician"] = technician
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )
    else:
        try:
            if appointment_id is not None:
                appointment = Appointment.objects.get(id=appointment_id)
                appointment.delete()
                response = JsonResponse({"message": "Deleted successfully"})
                response.status_code = 200
                return response
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    return JsonResponse(
        {"appointments": appointments},
        encoder=AppointmentListEncoder,
        safe=False
    )

@require_http_methods(["PUT"])
def update_appointment_status(request, appointment_id, status):
    if request.method == "PUT":
        statuses = {
            'finish': 'Finished',
            'cancel': 'Cancelled'
        }
        appointment = Appointment.objects.get(id=appointment_id)
        appointment.status = statuses[status]
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )
