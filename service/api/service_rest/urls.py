from django.urls import path
from .views import list_technicians, list_appointments, update_appointment_status

urlpatterns = [
    path('technicians/', list_technicians, name='list_technicians'),
    path('technicians/<int:technician_id>/', list_technicians, name='list_technicians'),
    path('appointments/', list_appointments, name='list_appointments'),
    path('appointments/<int:appointment_id>/', list_appointments, name='list_appointments'),
    path('appointments/<int:appointment_id>/<str:status>/', update_appointment_status, name='update_appointment_status'),
]
